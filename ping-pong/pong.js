const axios = require('axios');
const express = require('express');

const app = express();

let serveurPing;

function getPing(){
    return axios.post("http://annuaire:7000/serveur-ping", {url: "pong:5000"})
    .then(response => {
        serveurPing = response.data;
    });
}

let ping = async () => {
    while(!serveurPing) await getPing();
    axios.get("http://"+serveurPing+"/pong")
    .catch((err) => console.log("There was no ping on "+serveurPing+" :("));
}

getPing();

ping();

app.get("/ping", async (req, res) => {
    console.log("ping");
    res.sendStatus(200);
    ping();
    
    return;
});

app.listen(5000);
