FROM node:latest
WORKDIR /usr/src/ping-pong
COPY package*.json ./

RUN npm install 

COPY annuaire.js .

EXPOSE 7000

CMD [ "node", "annuaire.js" ]