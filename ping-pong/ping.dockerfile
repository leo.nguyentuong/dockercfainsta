FROM node:latest
WORKDIR /usr/src/ping-pong
COPY package*.json ./

RUN npm install 

COPY ping.js .

EXPOSE 5001

CMD [ "node", "ping.js" ]