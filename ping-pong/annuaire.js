const axios = require('axios');
const express = require('express');

const app = express();

let serveurPing, serveurPong;

app.use(express.json());

app.post('/serveur-ping', (req, res) => {
    console.log("Requête ping");
    serveurPong = req.body.url;
    return res.send(serveurPing);
});

app.post('/serveur-pong', (req, res) => {
    console.log("Requête pong");
    serveurPing = req.body.url;
    return res.send(serveurPong);
});

app.listen(7000, () => {
    console.log("Annuaire ouvert sur le port 7000");
});
