const axios = require('axios');
const express = require('express');

const app = express();

let serveurPong;

function getPong(){
    return axios.post("http://annuaire:7000/serveur-pong", {url: "ping:5001"})
    .then(response => {
        serveurPong = response.data;
    });
}

getPong();

app.get("/pong", async (req, res) => {
    console.log("pong");
    while(!serveurPong) await getPong();

    res.sendStatus(200);
    axios.get('http://'+serveurPong+'/ping')
    .catch((err) => console.log("There was no pong :("));

});

app.listen(5001);