FROM node:latest
WORKDIR /usr/src/ping-pong
COPY package*.json ./

RUN npm install 

COPY pong.js .

EXPOSE 5000

CMD [ "node", "pong.js" ]