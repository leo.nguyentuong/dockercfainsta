docker stop planner
docker rm planner

docker build -t eval/planner .
docker run --name planner --network=eval -e TASKS=4 eval/planner
